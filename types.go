package gcalendar

import (
	calendar "google.golang.org/api/calendar/v3"
)

var (
	Calendar map[string]GiteCalendar
	cal      *calendar.Service
	err      error
)

type GiteCalendar struct {
	Initial    string
	GiteName   string
	CalendarID string
}

type Event struct {
	ClientMail string
	ClientNom  string
	EndDate    string // Format 2024-05-17T15:04:05Z
	Initial    string
	PrivateMap map[string]string
	StartDate  string // Format 2024-05-17T15:04:05Z
}

func New() Event {
	e := Event{}
	e.PrivateMap = make(map[string]string)
	return e
}

func makeCalendar() {
	Calendar = make(map[string]GiteCalendar)
	Calendar["to"] = GiteCalendar{
		Initial:    "to",
		GiteName:   "La Topaze d'Or",
		CalendarID: "ed9bbb7add411bd822fd025b3e69b9a145d6457214fd899ef3ce0e753736f3fb@group.calendar.google.com",
	}
	Calendar["js"] = GiteCalendar{
		Initial:    "js",
		GiteName:   "Le Joyeux Saphir",
		CalendarID: "e1a4df2c36cd038a641ab3829cd36f55444720e6093b69fc4bdd84157137b224@group.calendar.google.com",
	}
	Calendar["ra"] = GiteCalendar{
		Initial:    "ra",
		GiteName:   "Le Rubis Ardent",
		CalendarID: "85b30747263791c986c4b03e2300bdfb2bb42bbb1d5ac307415fc2ac80dac5fe@group.calendar.google.com",
	}
	Calendar["be"] = GiteCalendar{
		Initial:    "be",
		GiteName:   "La Belle Emeraude",
		CalendarID: "227213d5d4130aafddd556aa503ede331a6b0c74745a19f84e96875bf6c7806d@group.calendar.google.com",
	}
	Calendar["jp"] = GiteCalendar{
		Initial:    "jp",
		GiteName:   "Le Jupiter",
		CalendarID: "9fd346dbdf2a4e66e8728cabc937fa53f9163347b205c2a667c242cf6c5e9c0a@group.calendar.google.com",
	}
}
