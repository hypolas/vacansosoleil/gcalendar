package gcalendar

import (
	"encoding/base64"
	"log"
	"os"
	"testing"
	"time"
)

func TestCreateEvent(t *testing.T) {
	for key := range Calendar {
		log.Println(key)
		createEnv := New()
		createEnv.Initial = key
		createEnv.StartDate = "2024-05-16T15:04:05Z"
		createEnv.EndDate = "2024-05-18T15:04:05Z"

		createEnv.CalCreateEvent()
	}
}

func TestServiceCred(t *testing.T) {
	service_auth, _ := os.ReadFile("service_base64.txt")
	servi, _ := base64.StdEncoding.DecodeString(string(service_auth))
	log.Println(string(servi))
}

func TestDateFormat(t *testing.T) {
	d := "23/02/1982"
	tt, err := time.Parse("02/01/2006", d)

	if err != nil {
		log.Println(err)
	}

	log.Println(tt.Format(time.RFC3339))
}

func TestListEvent(t *testing.T) {
	for key := range Calendar {
		log.Println(key)
		createEnv := New()
		createEnv.Initial = key

		createEnv.GetEvents()
	}
}
