package gcalendar

import (
	"context"
	"encoding/base64"
	"log"
	"os"

	contactType "gitlab.com/hypolas/vacansosoleil/go-types"
	calendar "google.golang.org/api/calendar/v3"
	"google.golang.org/api/option"
)

func init() {
	makeCalendar()
	auth, _ := base64.StdEncoding.DecodeString(os.Getenv("SERVICE_GOOGLE_AUTH"))
	cal, err = calendar.NewService(ctx, option.WithCredentialsJSON(auth))
	if err != nil {
		log.Fatalln(err)
	}
}

var (
	ctx context.Context = context.Background()
)

func (ce *Event) CalCreateEvent(client ...contactType.Contact) error {
	var cl contactType.Contact
	if len(client) > 0 {
		cl = client[0]
	}

	summary := ""
	if cl.Progress != "" {
		summary = Calendar[ce.Initial].GiteName + ": " + ce.ClientNom + ". Payé: " + cl.Progress + "."
	} else {
		summary = Calendar[ce.Initial].GiteName + ": " + ce.ClientNom + "."
	}

	private := make(map[string]string)
	private["ID"] = cl.ID
	private["progress"] = cl.Progress
	_, err := cal.Events.Insert(Calendar[ce.Initial].CalendarID, &calendar.Event{
		Summary:     summary,
		Description: "Location",
		Start: &calendar.EventDateTime{
			DateTime: ce.StartDate,
		},
		End: &calendar.EventDateTime{
			DateTime: ce.EndDate,
		},
		Status:       "tentative",
		Transparency: "transparent",
		ColorId:      "8",
		ExtendedProperties: &calendar.EventExtendedProperties{
			Private: private,
		},
	}).SendUpdates("all").Do()

	if err != nil {
		return err
	}
	return nil
}

func (ce *Event) GetEvents() error {
	events, err := cal.Events.List(Calendar[ce.Initial].CalendarID).Do()
	if err != nil {
		return err
	}

	for _, en := range events.Items {
		log.Println(en.Summary)
	}

	return nil
}
